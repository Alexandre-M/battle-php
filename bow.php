<?php

require_once("./weapon.php");

class bow extends weapon{

    private $range;

    function __construct($nameWeapon, $damage, $range){
        parent::__construct($nameWeapon, $damage);
        $this->range = $range;
    }

    function weaponHand(){
        echo "This weapon is ".$this->nameWeapon." with damage ".$this->damage." and range ".$this->range.".\n";
    }
}