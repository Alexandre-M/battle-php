<?php

abstract class weapon{
    protected $nameWeapon;
    public $damage;

    function __construct($nameWeapon, $damage){
        $this->nameWeapon = $nameWeapon;
        $this->damage = $damage;
    }

    function getName(){
        return $this->nameWeapon;
    }

    function getDamage(){
        return $this->damage;
    }

    abstract function weaponHand();
}